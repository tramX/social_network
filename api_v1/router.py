from social_network.router_api_v1 import router

from api_v1 import views

web_portal_router = router.register('posts', views.PostViewSet, base_name='posts')
web_portal_router = router.register('like_post', views.LikePostViewSet, base_name='like_post')
