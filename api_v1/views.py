from django.contrib.auth.models import User

from rest_framework import generics, permissions, status
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response

from api_v1 import serializers
from api_v1.models import Post


class Registration(generics.CreateAPIView):
    queryset = User
    serializer_class = serializers.UserSerializer


class UserInfo(generics.RetrieveAPIView):
    queryset = User
    serializer_class = serializers.UserInfoSerializer


class PostViewSet(ModelViewSet):
    serializer_class = serializers.PostSerializer
    queryset = Post.objects.all()
    permission_classes = (permissions.IsAuthenticated,)

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.author != request.user and kwargs.get('like') == None:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        return super().update(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        self.serializer_class = serializers.PostListGetSerializer
        return super().retrieve(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.author != request.user:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        return super().destroy(request, *args, **kwargs)

    def list(self, request, *args, **kwargs):
        self.serializer_class = serializers.PostListGetSerializer
        return super().list(self, request, *args, **kwargs)


class LikePostViewSet(PostViewSet):
    def perform_update(self, serializer):
        super().perform_update(serializer)
        if self.request.user.id in list(self.get_object().like.values_list('id', flat=True)):
            self.get_object().like.remove(self.request.user)
        else:
            self.get_object().like.add(self.request.user)


    def update(self, request, *args, **kwargs):
        self.serializer_class = serializers.PostLikeSerializer
        kwargs.update({'like': True})
        return super().update(request, *args, **kwargs)



