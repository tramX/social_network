from django.db import models
from django.contrib.auth.models import User


class Post(models.Model):
    title = models.CharField(max_length=255)
    text = models.TextField()
    author = models.ForeignKey(User, related_name='posts')
    like = models.ManyToManyField(User, related_name='post_likes', blank=True)

    def __str__(self):
        return '{}'.format(self.title)
