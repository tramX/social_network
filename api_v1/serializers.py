from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from rest_framework.exceptions import ValidationError

from pyhunter import PyHunter
import clearbit

from api_v1 import secret_keys
from api_v1.models import Post


class PyHunterValidator:
    message = _('Enter a valid email address PyHunterValidator.')

    def __init__(self):
        self.hunter = PyHunter(secret_keys.pyhunter_secret_key)

    def __call__(self, value):
        try:
            self.hunter.email_verifier(value)
        except:
            raise ValidationError(self.message)


class UserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
        required=True,
        validators=[UniqueValidator(queryset=User.objects.all()), PyHunterValidator()]
    )
    password = serializers.CharField(min_length=8)

    def create(self, validated_data):
        clearbit.key = secret_keys.clearbit_secret_key
        try:
            user_info = dict(clearbit.Enrichment.find(email=validated_data['email'], stream=True))
            first_name = user_info.get('person').get('name').get('familyName')
            last_name = user_info.get('person').get('name').get('givenName')
        except:
            first_name = ''
            last_name = ''
        user = User.objects.create_user(validated_data['email'], validated_data['email'],
                                        validated_data['password'], is_active=True, is_staff=True,
                                        first_name=first_name, last_name=last_name)
        return user

    class Meta:
        model = User
        fields = ('id', 'email', 'password')


class UserInfoSerializer(serializers.ModelSerializer):
    posts = serializers.SerializerMethodField()
    likes = serializers.SerializerMethodField()

    @staticmethod
    def get_posts(instance):
        return instance.posts.count()

    @staticmethod
    def get_likes(instance):
        return instance.post_likes.count()

    class Meta:
        model = User
        fields = ('id', 'email', 'posts', 'likes')


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = ('id', 'title', 'text')


class PostListGetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = ('id', 'title', 'text', 'author', 'like')


class PostLikeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = ('id', 'like')
