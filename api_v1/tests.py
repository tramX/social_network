from rest_framework.test import APITestCase, APITransactionTestCase
from django.contrib.auth import get_user_model
from django.utils.crypto import get_random_string

from rest_framework.reverse import reverse
import json
import os
import random

from api_v1 import models

user_model = get_user_model()


class SocialNetworkTestCase(APITestCase, APITransactionTestCase):
    def setUp(self):
        super().setUp()

        self.user_email = 'podwar2008@gmail.com'
        self.user_password = 'verylongpassword'

    def test_registration(self):
        new_user = self.client.post(reverse('api-auth:registrations'),
                                    {'email': self.user_email, 'password': self.user_password})
        self.assertEqual(new_user.status_code, 201)

    def test_login(self):
        new_user = self.client.post(reverse('api-auth:registrations'),
                                    {'email': self.user_email, 'password': self.user_password})
        self.assertEqual(new_user.status_code, 201)
        auth_user = self.client.post(reverse('api-auth:api-token-auth'),
                                     {'username': self.user_email, 'password': self.user_password})

        self.assertNotEqual(auth_user.json().get('token'), None)

    def test_posts(self):
        new_user = self.client.post(reverse('api-auth:registrations'),
                                    {'email': self.user_email, 'password': self.user_password})
        self.assertEqual(new_user.status_code, 201)
        auth_user = self.client.post(reverse('api-auth:api-token-auth'),
                                     {'username': self.user_email, 'password': self.user_password})

        self.client.credentials(HTTP_AUTHORIZATION='JWT {}'.format(auth_user.json().get('token')))
        new_post = self.client.post(reverse('api_v1:posts-list'), {'title': 'post1', 'text': 'post1 text'})
        self.assertEqual(new_post.status_code, 201)
        self.assertEqual(len(self.client.get(reverse('api_v1:posts-list')).json()), 1)

    def test_bot(self):
        with open('{}/bot.json'.format(os.path.dirname(os.path.abspath(__file__)))) as json_data:
            bot_config = json.load(json_data)

        self.user_list = []

        while 1:
            email = '{}@gmail.com'.format(get_random_string(length=12, allowed_chars='abcdefghijklmnopqrstuvwxyz'))
            new_user = self.client.post(reverse('api-auth:registrations'),
                                        {'email': email, 'password': '1qazxsw2'})
            if new_user.status_code == 201:
                auth_user = self.client.post(reverse('api-auth:api-token-auth'),
                                             {'username': email, 'password': '1qazxsw2'})

                self.user_list.append(
                    {'id': new_user.json().get('id'), 'email': email, 'token': auth_user.json().get('token')})

            if len(self.user_list) >= bot_config.get('number_of_users'):
                break

        self.assertEqual(len(self.user_list), models.User.objects.all().count())

        for user in self.user_list:
            self.client.credentials(HTTP_AUTHORIZATION='JWT {}'.format(user.get('token')))
            for i in range(0, random.randint(1, bot_config.get('max_posts_per_user'))):
                self.client.post(reverse('api_v1:posts-list'), {'title': 'title', 'text': 'text'})

        no_zero_like_posts = []

        while 1:
            posts = self.client.get(reverse('api_v1:posts-list')).json()
            user = random.choice(self.user_list)
            post = random.choice(posts)

            user_like_count = self.client.get(reverse('api-auth:user-info', args=(user.get('id'),))).json().get('likes')

            if post.get('author') != user.get('id') and user.get('id') not in post.get(
                    'like') and user_like_count < bot_config.get('max_likes_per_user'):
                self.client.credentials(HTTP_AUTHORIZATION='JWT {}'.format(user.get('token')))
                res = self.client.put(reverse('api_v1:like_post-detail', args=(post.get('id'),)))
                if res.status_code == 200 and post.get('id') not in no_zero_like_posts:
                    no_zero_like_posts.append(post.get('id'))

            if len(no_zero_like_posts) == len(posts):
                break

        for i in self.client.get(reverse('api_v1:posts-list')).json():
            self.assertNotEqual(len(i.get('like')), 0)
            self.assertNotIn(i.get('author'), i.get('like'))
