from django.contrib import admin

from api_v1.models import Post

admin.site.register(Post)
