from django.conf.urls import url

from api_v1 import views
from rest_framework_jwt.views import obtain_jwt_token

urlpatterns = [
    url(r'^registrations/$', views.Registration.as_view(), name='registrations'),
    url(r'^user-info/(?P<pk>[0-9]+)/$', views.UserInfo.as_view(), name='user-info'),
    url(r'^api-token-auth/', obtain_jwt_token, name='api-token-auth'),
]
